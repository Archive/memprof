# Traditional Chinese Messages for the memprof
# Copyright (C) 2001-2004 Free Software Foundation, Inc.
# He Qiangqiang <carton@263.net>, 2001
# Wei-Lun Chao <chaoweilun@pcmail.com.tw>, 2004, 05
# Jim Huang <jserv@0xlab.org>, 2009
#
msgid ""
msgstr ""
"Project-Id-Version: MemProf 0.5.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2009-11-26 16:25+0800\n"
"PO-Revision-Date: 2005-04-08 00:57+0800\n"
"Last-Translator: Jim Huang <jserv@0xlab.org>\n"
"Language-Team: Chinese <zh-l10n@linux.org.tw>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8-bit\n"

#: ../src/leakdetect.c:496 ../src/profile.c:708
#, c-format
msgid "Cannot open output file: %s\n"
msgstr "無法打開輸出檔案：%s\n"

#: ../src/main.c:772
#, c-format
msgid "Execution of \"%s\" failed: %s"
msgstr "執行「%s」失敗： %s"

#: ../src/main.c:902
msgid "MemProf"
msgstr "MemProf"

#: ../src/main.c:1013
#, c-format
msgid "Cannot find executable for \"%s\""
msgstr "找不到「%s」的可執行檔案"

#: ../src/main.c:1287
msgid "MemProf Error"
msgstr "MemProf 錯誤"

#: ../src/main.c:1287
msgid "MemProf Warning"
msgstr "MemProf 警告"

#: ../src/main.c:1350
msgid "Functions"
msgstr "函式"

#: ../src/main.c:1351 ../src/main.c:1368 ../src/main.c:1386
msgid "Self"
msgstr "自身行程"

#: ../src/main.c:1352 ../src/main.c:1387
msgid "Total"
msgstr "總和"

#: ../src/main.c:1367
msgid "Descendants"
msgstr "從屬者"

#: ../src/main.c:1369
msgid "Cumulative"
msgstr "累計"

#: ../src/main.c:1385
msgid "Callers"
msgstr "呼叫者"

#: ../src/main.c:1412
msgid "Address"
msgstr "位址"

#: ../src/main.c:1413
msgid "Size"
msgstr "大小"

#: ../src/main.c:1414
msgid "Caller"
msgstr "呼叫者"

#: ../src/main.c:1434
msgid "Function"
msgstr "函式"

#: ../src/main.c:1435
msgid "Line"
msgstr "列數"

#: ../src/main.c:1436
msgid "File"
msgstr "檔案"

#: ../src/main.c:1644
msgid "Really detach from finished process?"
msgstr "真的從已完成的行程中脫離？"

#: ../src/main.c:1646
msgid "Really detach from running process?"
msgstr "真的從正在執行的行程中脫離？"

#: ../src/main.c:1676
msgid "Really kill running process?"
msgstr "真的要砍掉正在執行的行程？"

#: ../src/main.c:1712
msgid "Create new windows for forked processes"
msgstr "為 fork 出的行程建立新視窗"

#: ../src/main.c:1714
msgid "Retain windows for processes after exec()"
msgstr "在行程執行了 exec() 後保留視窗"

#: ../src/main.c:1716
msgid "Type of profiling information to collect"
msgstr "要收集的摘要資訊類型"

#: ../src/main.c:1718
msgid "Number of samples/sec for time profile (1k=1000)"
msgstr "時間摘要中每秒取樣數目 (1k=1000)"

#: ../src/main.c:1720
msgid "Functions allocating memory"
msgstr "配置記憶體的函式"

#: ../src/main.c:1805
#, c-format
msgid "Argument of --profile must be one of 'memory', 'cycles', or 'time'\n"
msgstr "--profile 的引數必須是 'memory'、'cycles' 或 'time' 其中之一\n"

#: ../src/main.c:1842
msgid "Cannot find memprof.glade"
msgstr "找不到 memprof.glade"

#: ../memprof.glade.h:1
msgid "# of Allocations: "
msgstr "分配記憶體次數："

#: ../memprof.glade.h:2
msgid "0"
msgstr "0"

#: ../memprof.glade.h:3
msgid "0k"
msgstr "0k"

#: ../memprof.glade.h:4
msgid "32k"
msgstr "32k"

#: ../memprof.glade.h:5
msgid "Adding Skip Function"
msgstr "添加要忽略的函式"

#: ../memprof.glade.h:6
msgid "Adding Skip Regular Expression"
msgstr "添加要忽略的正規表示式"

#: ../memprof.glade.h:7
msgid "Bytes / Allocation: "
msgstr "位元組數/分配記憶體："

#: ../memprof.glade.h:8
msgid "Check for Leaks"
msgstr "檢查記憶體漏失"

#: ../memprof.glade.h:9
msgid "Command to run on double click in stack trace:"
msgstr "在堆疊追蹤時連按兩下所執行的命令："

#: ../memprof.glade.h:10
msgid "Create Profile"
msgstr "建立記憶體使用摘要"

#: ../memprof.glade.h:11
msgid "Defaults"
msgstr "預設值"

#: ../memprof.glade.h:12
msgid "Draw the memory map"
msgstr "繪製記憶體圖例"

#: ../memprof.glade.h:13
msgid ""
"Enter a regular expression of function names to skip when computing profiles"
msgstr "輸入在計算記憶體摘要時忽略的函式名之正規表示式"

#: ../memprof.glade.h:14
msgid "Enter pathname to an executable"
msgstr "輸入可執行檔案的路徑名"

#: ../memprof.glade.h:15
msgid "Enter the name of a function to skip when computing profiles"
msgstr "輸入在計算記憶體使用情況時要忽略的函式"

#: ../memprof.glade.h:16
msgid "Functions to Skip:"
msgstr "要忽略的函式："

#: ../memprof.glade.h:17 ../src/tree.c:109
msgid "Kill"
msgstr "砍掉"

#: ../memprof.glade.h:18
msgid "Kill Program"
msgstr "砍掉程式"

#: ../memprof.glade.h:19
msgid "Leak Detection Options"
msgstr "偵測記憶體漏失選項"

#: ../memprof.glade.h:20
msgid "Leaks"
msgstr "記憶體漏失"

#: ../memprof.glade.h:21
msgid "MemProf - Processes"
msgstr "MemProf - 行程"

#: ../memprof.glade.h:22
msgid "Memory Usage Maps"
msgstr "記憶體使用圖例"

#: ../memprof.glade.h:23
msgid "Preferences"
msgstr "偏好設定"

#: ../memprof.glade.h:24
msgid "Process _Tree"
msgstr "行程樹(_T)"

#: ../memprof.glade.h:25
msgid "Profile"
msgstr "記憶體使用"

#: ../memprof.glade.h:26
msgid "Profile Options"
msgstr "記憶體使用摘要選項"

#: ../memprof.glade.h:27
msgid "Record"
msgstr "紀錄"

#: ../memprof.glade.h:28
msgid "Regular expressions to Skip:"
msgstr "要忽略的函式名正規表示式："

#: ../memprof.glade.h:29
msgid "Reset"
msgstr "重置"

#: ../memprof.glade.h:30
msgid "Run"
msgstr "執行"

#: ../memprof.glade.h:31
msgid "Run Executable"
msgstr "執行可執行檔案"

#: ../memprof.glade.h:32
msgid "Run Program"
msgstr "執行程序"

#: ../memprof.glade.h:33
msgid "Save"
msgstr "保存"

#: ../memprof.glade.h:34
msgid "Save Report"
msgstr "保存報告"

#: ../memprof.glade.h:35
msgid "Select an Executable"
msgstr "選擇一個執行可執行檔"

#: ../memprof.glade.h:36
msgid "Stack Trace"
msgstr "堆疊追蹤"

#: ../memprof.glade.h:37
msgid "Time Graph"
msgstr "時序圖"

#: ../memprof.glade.h:38
msgid "Total Bytes: "
msgstr "總位元組總數："

#: ../memprof.glade.h:39
msgid "_Detach"
msgstr "脫離(_D)"

#: ../memprof.glade.h:40
msgid "_File"
msgstr "檔案(_F)"

#: ../memprof.glade.h:41
msgid "_Help"
msgstr "求助(_H)"

#: ../memprof.glade.h:42
msgid "_Process"
msgstr "行程(_P)"

#: ../src/process.c:839
msgid "Initial"
msgstr "初始化"

#: ../src/process.c:842
msgid "Starting"
msgstr "正在開始"

#: ../src/process.c:845
msgid "Running"
msgstr "正在執行"

#: ../src/process.c:848
msgid "Exiting"
msgstr "正在退出"

#: ../src/process.c:851 ../src/process.c:854
msgid "Defunct"
msgstr "已死行程"

#: ../src/server.c:289
#, c-format
msgid "Cannot find %s"
msgstr "找不到「%s」"

#: ../src/tree.c:102
msgid "Show"
msgstr "顯示"

#: ../src/tree.c:105
msgid "Hide"
msgstr "隱藏"

#: ../src/tree.c:112
msgid "Detach"
msgstr "脫離"

#: ../src/tree.c:180
msgid "PID"
msgstr "行程編號"

#: ../src/tree.c:181
msgid "Command Line"
msgstr "命令列"

#: ../src/tree.c:182
msgid "Status"
msgstr "狀態"
